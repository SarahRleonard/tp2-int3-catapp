import React, { useState, useEffect } from "react";
import { ThemeProvider } from "@material-ui/styles";
import { theme } from "./main/theme/MaterialUITheme";
//import './App.css';
import CatScreen from "./cat/components/CatScreen";

class App extends React.Component { /*va chercher les class react au lieu des fonctions*/
  render(){
    return (
      
  <ThemeProvider theme={theme}>
    <CatScreen/>
  </ThemeProvider>
  
    );
  }
}

export default App;