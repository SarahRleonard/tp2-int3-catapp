import React, { useState, useEffect } from "react";
import App from '../../App.css';
import ReactPaginate from 'react-paginate';

const apiKey = '3bd22d1b-4d7f-4029-b40e-7ee167b9eb4b';
const url = 'https://api.thecatapi.com/v1/images/search?limit=10&page=5&order=DESC' //single image


//'https://api.thecatapi.com/v1/images/search?limit=5&page=10&order=DESC
//https://docs.thecatapi.com/authentication

function CatScreen() {
  //const [catUrl, setCatUrl] = useState('https://cdn2.thecatapi.com/images/eeg.jpg'); //default value ex: [] array
  const [cats, setCats] = useState([]);
  const [page, setPage] = useState(1);


  useEffect(
    function fetchCats() {

      fetch(url, {
        'x-api-key': apiKey
      })
        .then((res) => res.json())
        .then((cats) => {
          setCats(cats)
        })
        .catch((error) => {
          console.log('Error :', error);
        });

    }, [page]);



  const goToNextPage = () => {
    setPage(page + 1);
  };

  const goToPreviousPage = () => {
    setPage(page - 1);
  }


  console.log(cats);

  //JSX

  return (
    <div>
      <ul class="nav bg-secondary">
        <li class="nav-item">
          <a class="nav-link active text-dark" aria-current="page" href="/">Browser Cat</a>
        </li>
      </ul>

      <div className="container flex">
        {/* <ReactPaginate
          previousLabel={'precedente'}
          nextLabel={'Suivante'}
          breakLabel={'...'}
          pageCount={8}
          marginPagesDisplayed={3}
          pageRangeDisplayed={3}
          OnPageChange={page}
          containerClassName={'pagination justify-content-center'}
          pageClassName={'page-item'}
          pageLinkClassName={'page-link'}
          previousClassName={'page-item'}
          previousLinkClassName={'page-link'}
          nextClassName={'page-item'}
          nextLinkClassName={'page-link'}
          breakLinkClassName={'page-link'}
          activeClassName={'active'}
        /> */}


        <div>
          <div className="row m-2">

            {cats.map((cat) => {
              return (

                <div className="col-sm6 col-md-4 v my-2">
                  <div className="row card text-center w-100 h-100">
                    <p class="card-text center">Cat id # {cat.id}</p>
                    <img src={cat.url} alt="cat.id" className="rounded image-fluid" />
                  </div>
                </div>
              )
            })}
            {/* <span> Page actuelle : {page}</span> */}


          </div>
        </div>
        <nav aria-label="...">
          <ul class="pagination">
            <li class="page-item">
              <a class="page-link" onClick={goToPreviousPage} >Previous</a>
            </li>
            <li class="page-item"><a class="page-link">1</a></li>
            <li class="page-item " aria-current={page}>
              <a class="page-link" >2</a>
            </li>
            <li class="page-item">
              <a class="page-link" >3</a></li>
            <li class="page-item">
              <a class="page-link" onClick={goToNextPage} >Next</a>
            </li>
          </ul>
        </nav>
        {/* <button className="page-item page-link" onClick={goToPreviousPage}>Aller à la page précédente</button>
        <button className="page-item page-link" onClick={goToNextPage}>Aller à la page suivante</button> */}
      </div>
    </div>

  );

}



export default CatScreen;